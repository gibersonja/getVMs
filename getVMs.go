package main

import (
	"bufio"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
	"time"

	"golang.org/x/term"
)

var outputFile string

type virtualMachine struct {
	name   string
	id     string
	tagIDs []string
	tags   []string
}

type tagsStruct struct {
	name string
	id   string
}

func main() {
	exe, err := os.Executable()
	er(err)

	if len(os.Args) < 2 {
		er(fmt.Errorf("must specify at least one argument"))
	}

	var vcenters []string
	var vms []virtualMachine
	var tags []tagsStruct

	args := os.Args[1:]

	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "-h" || arg == "--help" {
			//print help
			fmt.Print("-h | --help\tPrint this help message\n")
			fmt.Print("--output=<FILE>\tPrint output to file as well as STDOUT\n")
			fmt.Print("<FQDN|IP>\tvCenter address to connect to\n\n")
			fmt.Printf("EXAMPLE:\n\t%s <FQDN1> <IP> <FQDN2> --output=OUT_FILE\n", filepath.Base(exe))
			return
		}

		regex := regexp.MustCompile(`--output=(\S+)`)
		if regex.MatchString(arg) {
			outputFile = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`^https:\S+`)
		if regex.MatchString(arg) {
			vcenters = append(vcenters, arg)
		}
	}

	for i := 0; i < len(vcenters); i++ {
		vcenter := vcenters[i]

		charset := "abcdefghijklmnopqrstuvwxyz"

		fmt.Printf("Authenticating %s\n", vcenter)
		fmt.Print("Username: ")
		reader := bufio.NewReader(os.Stdin)
		username, _ := reader.ReadString('\n')
		username = strings.Replace(username, "\r", "", -1)
		username = strings.Replace(username, "\n", "", -1)
		fmt.Print("Password: ")
		bytepass, err := term.ReadPassword(int(os.Stdin.Fd()))
		password := string(bytepass)

		for n := 0; n < len(bytepass); n++ {
			bytepass[n] = byte(charset[rand.Intn(len(charset))])
		}

		auth := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", username, password)))

		password = ""
		var stringOverwrite []byte
		for n := 0; n < 128; n++ {
			stringOverwrite = append(stringOverwrite, charset[rand.Intn(len(charset))])
		}
		password = string(stringOverwrite)

		fmt.Print("\n\n")

		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

		client := http.Client{Timeout: 5 * time.Second}

		req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/api/session", vcenter), http.NoBody)
		er(err)

		req.Header.Add("authorization", fmt.Sprintf("Basic %s", auth))

		auth = ""
		var authOverwrite []byte
		for n := 0; n < 128; n++ {
			authOverwrite = append(authOverwrite, charset[rand.Intn(len(charset))])
		}
		auth = string(authOverwrite)

		res, err := client.Do(req)
		er(err)

		if res.StatusCode < 200 || res.StatusCode > 299 {
			er(fmt.Errorf(res.Status))
		}

		defer res.Body.Close()

		resBody, err := io.ReadAll(res.Body)
		er(err)

		sessionID := string(resBody[1 : len(resBody)-1])

		req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/rest/vcenter/vm", vcenter), http.NoBody)
		er(err)

		req.Header.Add("vmware-api-session-id", sessionID)

		res, err = client.Do(req)
		er(err)

		if res.StatusCode < 200 || res.StatusCode > 299 {
			er(fmt.Errorf(res.Status))
		}

		defer res.Body.Close()

		resBody, err = io.ReadAll(res.Body)

		type vmsResp struct {
			Value []struct {
				Name            string
				Vm              string
				Memory_size_MiB int
				Power_state     string
				Cpu_count       int
			}
		}

		var vmsData vmsResp

		err = json.Unmarshal(resBody, &vmsData)
		er(err)

		for i := 0; i < len(vmsData.Value); i++ {
			var tmp virtualMachine
			tmp.name = vmsData.Value[i].Name
			tmp.id = vmsData.Value[i].Vm

			vms = append(vms, tmp)
		}

		req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/cis/tagging/tag", vcenter), http.NoBody)
		er(err)

		req.Header.Add("vmware-api-session-id", sessionID)

		res, err = client.Do(req)
		er(err)

		if res.StatusCode < 200 || res.StatusCode > 299 {
			er(fmt.Errorf(res.Status))
		}

		defer res.Body.Close()

		resBody, err = io.ReadAll(res.Body)

		var tagIDs []string

		err = json.Unmarshal(resBody, &tagIDs)
		er(err)

		for i := 0; i < len(tagIDs); i++ {
			tagID := tagIDs[i]

			req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/cis/tagging/tag/%s", vcenter, tagID), http.NoBody)
			er(err)

			req.Header.Add("vmware-api-session-id", sessionID)

			res, err = client.Do(req)
			er(err)

			if res.StatusCode < 200 || res.StatusCode > 299 {
				er(fmt.Errorf(res.Status))
			}

			defer res.Body.Close()

			resBody, err = io.ReadAll(res.Body)

			type tagging struct {
				Category_id string
				Description string
				Id          string
				Name        string
				Used_by     []string
			}

			var tagNames tagging

			err = json.Unmarshal(resBody, &tagNames)
			er(err)

			var tmp2 tagsStruct
			tmp2.id = tagNames.Id
			tmp2.name = tagNames.Name

			tags = append(tags, tmp2)

		}

		req, err = http.NewRequest(http.MethodGet, fmt.Sprintf("%s/api/vcenter/tagging/associations", vcenter), http.NoBody)
		er(err)

		req.Header.Add("vmware-api-session-id", sessionID)

		res, err = client.Do(req)
		er(err)

		if res.StatusCode < 200 || res.StatusCode > 299 {
			er(fmt.Errorf(res.Status))
		}

		defer res.Body.Close()

		resBody, err = io.ReadAll(res.Body)

		type association struct {
			Associations []struct {
				Object struct {
					Id   string
					Type string
				}
				Tag string
			}
			Marker string
			Status string
		}

		var associations association

		err = json.Unmarshal(resBody, &associations)
		er(err)

		for i := 0; i < len(associations.Associations); i++ {
			vmid := associations.Associations[i].Object.Id
			for j := 0; j < len(vms); j++ {
				if vms[j].id == vmid {
					vms[j].tagIDs = append(vms[j].tagIDs, associations.Associations[i].Tag)
				}
			}

		}

	}

	for i := 0; i < len(vms); i++ {
		for j := 0; j < len(vms[i].tagIDs); j++ {
			for k := 0; k < len(tags); k++ {
				if vms[i].tagIDs[j] == tags[k].id {
					vms[i].tags = append(vms[i].tags, tags[k].name)
				}
			}
		}
	}

	for i := 0; i < len(tags); i++ {
		tag := tags[i]
		print(fmt.Sprintf("\n%s:\n", tag.name))
		for j := 0; j < len(vms); j++ {
			vm := vms[j]
			for k := 0; k < len(vm.tags); k++ {
				vmTags := vm.tags[k]
				if tag.name == vmTags {
					print(fmt.Sprintf("  %s\n", vm.name))
				}
			}
		}
	}

	print(fmt.Sprint("\nNo Tag Assigned:\n"))
	for i := 0; i < len(vms); i++ {
		vm := vms[i]
		if 0 == len(vm.tags) {
			print(fmt.Sprintf("  %s\n", vm.name))
		}
	}
}

func print(input string) {
	fmt.Print(input)
	if outputFile != "" {
		fh, err := os.OpenFile(outputFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		er(err)
		defer fh.Close()

		_, err = fh.WriteString(input)
		er(err)
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
